const express = require('express');
const router = express.Router();

const controller = require('../controllers/teamMembers');

/* GET team members listing. */
router.get('/', function(req, res, next) {
    res.send('List of team members: ');
});

router.get('/', controller.getAll);
router.get('/:id', controller.getByID);
router.get('/getUserByRole', controller.getByRole);
router.post('/', controller.create);
router.put('/:id', controller.replace);
router.patch('/:id', controller.edit);
router.delete('/:id', controller.destroy);

module.exports = router;

